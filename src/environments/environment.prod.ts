export const environment = {
  production: true,
  _apiURL: 'https://humct-backend.azurewebsites.net/',
  _redirectUri: 'https://humct.azurewebsites.net/termcondition',
  _clientId: '043001d7-3511-4f9b-931f-603d307f228c',
  _authority: 'https://login.microsoftonline.com/common',
};
