import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import Swal from 'sweetalert2';
import { GeneralService } from '../services/general.service';
import { LocalStorageService } from '../services/local-storage.service';

@Component({
  selector: 'app-update-mct-form',
  templateUrl: './update-mct-form.component.html',
  styleUrls: ['./update-mct-form.component.scss'],
})
export class UpdateMctFormComponent implements OnInit {
  MCTActivitiesFormGroup!: FormGroup;
  isShow: number = 0;
  radiostatusInternal?: boolean = false;
  radiostatusExternal?: boolean = false;
  public id?: any;
  private userId: any;
  user: any;
  constructor(
    private _formBuilder: FormBuilder,
    private _generalService: GeneralService,
    private _Activatedroute: ActivatedRoute,
  ) {}

  ngOnInit(): void {
    this.id = this._Activatedroute.snapshot.paramMap.get('id');
    this.userId = this._Activatedroute.snapshot.paramMap.get('UserId');
    this.buildForm();
    // if(this.localStorageService.isLocalStorageSupported){
    this.getMctFormById(this.id);
    //   this.user = JSON.parse(this.localStorageService.get(this.UserId));
    // }
  }

  buildForm(): void {
    this.MCTActivitiesFormGroup = this._formBuilder.group({
      EventLocation: ['', Validators.required],
      MctBasicForm: this._formBuilder.group({
        CategoryName: [''],
        OtherCategory: [''],
        ActivityName: [''],
        OtherActivity: [''],
        Position: [''],
        OtherPosition: [''],
        FromMonth: ['', Validators.required],
        FromYear: ['', Validators.required],
        ToMonth: ['', Validators.required],
        ToYear: ['', Validators.required],
      }),
      ExternalBasicFrom: this._formBuilder.group({
        OrganizationName: [''],
        Countrydropdown: [''],
        Statedropdown: [''],
        Citydropdown: [''],
        WebAddress: [''],
        Image: [''],
      }),
      InternalBasicFrom: this._formBuilder.group({
        SupervisorName: [''],
        SupervisorDepartment: [''],
        SupervisorEmail: [''],
        InternalFormImage: [''],
      }),
    });
  }

  getMctFormById(id: any) {
    this._generalService.getMctFormById(id).subscribe((data: any) => {
      if (Array.isArray(data) && data.length) {
        if (data[0].internalExternal == 1) {
          this.activityLocationInternal(data[0].internalExternal);
          this.MCTActivitiesFormGroup.controls.EventLocation.patchValue(
            data[0].internalExternal
          );
        
        } else {
          this.activityLocationExternal(data[0].internalExternal);
          this.MCTActivitiesFormGroup.controls.EventLocation.patchValue(2);
        }
        data.forEach((element) => {
          this.MCTActivitiesFormGroup.controls.MctBasicForm.patchValue({
            CategoryName: element.categoryId,
            ActivityName: element.activityId,
            Position: element.positionId,
            OtherCategory: element.otherCategory,
            OtherActivity: element.otherActivity,
            OtherPosition: element.otherPosition,
            FromMonth: element.fromMonth,
            FromYear: element.fromYearId,
            ToMonth: element.toMonth,
            ToYear: element.toYearId,
          });
          if (element.internalExternal == 1) {
            this.radiostatusInternal = true;
            data.forEach((element) => {
              this.MCTActivitiesFormGroup.controls.InternalBasicFrom.patchValue(
                {
                  SupervisorName: element['internalDTO'][0].supervisorName,
                  SupervisorDepartment:
                    element['internalDTO'][0].supervisorEmail,
                  SupervisorEmail:
                    element['internalDTO'][0].supervisorDepartment,
                }
              );
            });
          } else if (element.internalExternal == 2) {
            data.forEach((element) => {
              this.radiostatusExternal = true;
              this.MCTActivitiesFormGroup.controls.ExternalBasicFrom.patchValue(
                {
                  OrganizationName: element['externalDTO'][0].organizationName,
                  Countrydropdown: element['externalDTO'][0].countryId,
                  Statedropdown: element['externalDTO'][0].stateId,
                  Citydropdown: element['externalDTO'][0].cityId,
                  WebAddress: element['externalDTO'][0].webAddress,
                  Image: element['externalDTO'][0].image,
                }
              );
            });
          }
        });
      }
    });
  }

  activityLocationInternal(val: number): void {
    this.isShow = val;
    this.MCTActivitiesFormGroup.controls.ExternalBasicFrom.reset();
  }

  activityLocationExternal(val: number): void {
    this.isShow = val;
    this.MCTActivitiesFormGroup.controls.InternalBasicFrom.reset();
  }

  onUpdate() {
    if (this.MCTActivitiesFormGroup.controls.MctBasicForm.valid) {
      let countryId: number = 0;
      let stateId: number = 0;
      let cityId: number = 0;

      let categoryName =
        this.MCTActivitiesFormGroup.controls.MctBasicForm.value.CategoryName;
      let activityName =
        this.MCTActivitiesFormGroup.controls.MctBasicForm.value.ActivityName;
      let positionName =
        this.MCTActivitiesFormGroup.controls.MctBasicForm.value.Position;

      if (categoryName === 'otherCategory') {
        categoryName = '0';
      }
      if (activityName === 'OtherActivity') {
        activityName = '0';
      }
      if (positionName === 'OtherPosition') {
        positionName = '0';
      }

      if (
        this.MCTActivitiesFormGroup.controls.MctBasicForm.value.EventLocation !=
        '1'
      ) {
        countryId =
          this.MCTActivitiesFormGroup.controls.ExternalBasicFrom.value
            .Countrydropdown;
        stateId =
          this.MCTActivitiesFormGroup.controls.ExternalBasicFrom.value
            .Statedropdown;
        cityId =
          this.MCTActivitiesFormGroup.controls.ExternalBasicFrom.value
            .Citydropdown;
      }

      let MCTForm = {
        CategoryId: categoryName,
        OtherCategory:
          this.MCTActivitiesFormGroup.controls.MctBasicForm.value.OtherCategory,
        ActivityId: activityName,
        OtherActivity:
          this.MCTActivitiesFormGroup.controls.MctBasicForm.value.OtherActivity,
        PositionId: positionName,
        OtherPosition:
          this.MCTActivitiesFormGroup.controls.MctBasicForm.value.OtherPosition,
        FromMonth:
          this.MCTActivitiesFormGroup.controls.MctBasicForm.value.FromMonth,
        FromYearId:
          this.MCTActivitiesFormGroup.controls.MctBasicForm.value.FromYear,
        ToMonth:
          this.MCTActivitiesFormGroup.controls.MctBasicForm.value.ToMonth,
        ToYearId:
          this.MCTActivitiesFormGroup.controls.MctBasicForm.value.ToYear,
        InternalExternalEvent:
          this.MCTActivitiesFormGroup.controls.EventLocation.value,
        OrganizationName:
          this.MCTActivitiesFormGroup.controls.ExternalBasicFrom.value
            .OrganizationName,
        CountryId: countryId,
        StateId: stateId,
        CityId: cityId,
        WebAdress:
          this.MCTActivitiesFormGroup.controls.ExternalBasicFrom.value
            .WebAddress,
        Image:
          this.MCTActivitiesFormGroup.controls.ExternalBasicFrom.value.Image,
        SupervisorName:
          this.MCTActivitiesFormGroup.controls.InternalBasicFrom.value
            .SupervisorName,
        SupervisorDepartment:
          this.MCTActivitiesFormGroup.controls.InternalBasicFrom.value
            .SupervisorDepartment,
        SupervisorEmail:
          this.MCTActivitiesFormGroup.controls.InternalBasicFrom.value
            .SupervisorEmail,
        InternalFormImage:
          this.MCTActivitiesFormGroup.controls.InternalBasicFrom.value
            .InternalFormImage,
      };
      this._generalService
        .UpdateMCTForm(this.id, MCTForm)
        .subscribe((data: any) => {
          console.log(data);
          if (data['responseCode'] == '00') {
            Swal.fire({
              position: 'center',
              icon: 'success',
              title: 'Your record been updated',
              timer: 1500,
            });
             this.getMctFormById(this.id);
          } else {
            Swal.fire({
              position: 'center',
              icon: 'error',
              title: 'Request failed',
              timer: 1500,
            });
          }
        });
    } else {
      Swal.fire({
        icon: 'error',
        title: 'Oops...',
        text: 'some data are missing in Internal section!',
        footer: '<a href="">Why do I have this issue?</a>',
      });
    }
  }
}