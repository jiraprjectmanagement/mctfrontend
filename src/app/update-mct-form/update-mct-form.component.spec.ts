import { ComponentFixture, TestBed } from '@angular/core/testing';

import { UpdateMctFormComponent } from './update-mct-form.component';

describe('UpdateMctFormComponent', () => {
  let component: UpdateMctFormComponent;
  let fixture: ComponentFixture<UpdateMctFormComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ UpdateMctFormComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(UpdateMctFormComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
