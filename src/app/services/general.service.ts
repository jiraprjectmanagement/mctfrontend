import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { environment } from 'src/environments/environment';
import { MCTForm, PersonalInfo, SuccessResponse } from '../interface/igeneral';

const _baseURL = environment._apiURL;
const _controllerName = "general";

@Injectable({
  providedIn: "root",
})
export class GeneralService {
  constructor(private http: HttpClient) {}

  getAllYears() {
    return this.http.get<any>(
      environment._apiURL + _controllerName + "/GetAllYears/"
    );
  }
  getSupervisors() {
    return this.http.get<any>(
      environment._apiURL + _controllerName + "/GetAllSupervisor/"
    );
  }
  getPersonDataFromAD(email: string) {
    return this.http.get<any>(
      environment._apiURL + _controllerName + "/GetPersonDataFromAd/" + email
    );
  }
  CreatePersonalInfo(formData: PersonalInfo) {
    return this.http.post(
      environment._apiURL + _controllerName + "/CreatePersonalInfo/",
      formData
    );
  }
  getProfileIfExists(Id: any) {
    return this.http.get<any>(
      environment._apiURL + _controllerName + "/getProfileIfExists/" + Id
    );
  }
  getAllCountries() {
    return this.http.get<any>(
      environment._apiURL + _controllerName + "/getAllCountries"
    );
  }
  getAllStatesByCountryId(Id: any) {
    return this.http.get<any>(
      environment._apiURL + _controllerName + "/getAllStatesByCountryId/" + Id
    );
  }
  getAllCitiesByStateId(Id: any) {
    return this.http.get<any>(
      environment._apiURL + _controllerName + "/getAllCitiesByStateId/" + Id
    );
  }
  getAllEmpInfo() {
    return this.http.get<any>(
      environment._apiURL + _controllerName + "/GetAllEmpInfo"
    );
  }
  createMCTForm(req: MCTForm) {
    return this.http.post<any>(
      environment._apiURL + _controllerName + "/CreateMCTForm",
      req
    );
  }
  getMctFormByUserId(Id: number): Observable<any[]> {
    return this.http.get<any[]>(
      environment._apiURL + _controllerName + "/getMctFormByUserId/" + Id
    );
  }
  getMctFormById(Id: number): Observable<any[]> {
    return this.http.get<any[]>(
      environment._apiURL + _controllerName + "/getMctFormById/" + Id
    );
  }
  UpdateMCTForm(Id: number, req: MCTForm) {
    return this.http.put<any>(
      environment._apiURL + _controllerName + "/UpdateMCTForm/" + Id,
      req
    );
  }
}

