import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { environment } from 'src/environments/environment';
import { SuccessResponse } from '../interface/igeneral';

const _baseURL = environment._apiURL;
const _controllerName = "activity";

@Injectable({
  providedIn: 'root'
})
export class ActivityService {

  constructor(private http: HttpClient) { }

  getAllActivityByCategoryId(id:number){
    return this.http.get<any>(environment._apiURL + _controllerName + '/GetAllActivityByCategoryId/' + id);  
  }
  getAllActivity(){
    return this.http.get<any>(environment._apiURL + _controllerName + '/GetAllActivity');  
  }
  getAllActivityById(id:number){
    return this.http.get<any>(environment._apiURL + _controllerName + '/GetAllActivityById/' + id);  
  }
  getAllPosition(){
    return this.http.get<any>(environment._apiURL + _controllerName + '/GetAllPosition');  
  } 
  getAllPositionByCategoryId(id:number){
    return this.http.get<any>(environment._apiURL + _controllerName + '/GetPositionByCategoryId/' + id);  
  }
  DisableMctForm(id:number):Observable<any> {
    return this.http.get<any>(environment._apiURL + _controllerName + '/DisableMctForm/'+ id)
  }
}
