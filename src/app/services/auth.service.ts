import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';
import { User, UserDetail } from '../interface/igeneral';

const _baseURL = environment._apiURL;

const _controllerName = "Auth";
@Injectable({
  providedIn: "root",
})
export class AuthService {
  constructor(private http: HttpClient) {}

  CreateUserRecord(req: User) {
    return this.http.post<any>(
      environment._apiURL + _controllerName + "/CreateUserRecord",
      req
    );
  }
  getUserDetailsByEmail(email?:string){
      return this.http.get<any>(
        environment._apiURL +
          _controllerName +
          "/GetUserDetailsByEmail?email=" +
          email
      );
  }
}
