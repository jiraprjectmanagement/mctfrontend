import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from 'src/environments/environment';

const _baseURL = environment._apiURL;
const _controllerName = "Category";

@Injectable({
  providedIn: 'root'
})
export class CategoryService {

  constructor(private http: HttpClient) { }

   getAllCategory(){
    return this.http.get<any>(environment._apiURL + _controllerName + '/GetAllCategory');  
  } 
   getCategoryById(id:number){
    return this.http.get<any>(environment._apiURL + _controllerName + '/GetAllCategoryById/' + id);  
   }

}
