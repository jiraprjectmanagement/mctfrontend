import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';
import { IComment } from '../interface/comment';

const _controllerName = 'comment';

@Injectable({
  providedIn: 'root',
})
export class CommentService {
  constructor(private http: HttpClient) {}

  getAllCommentByMctFormId(id: number) {
    return this.http.get(
      environment._apiURL + _controllerName + '/getCommentByMctFormId/' + id
    );
  }
  ResolvedCommentById(id: number, req: IComment) {
    return this.http.put(
      environment._apiURL + _controllerName + '/ResolvedCommentById/' + id,req
    );
  }
  countComment(id:number){
     return this.http.get(
       environment._apiURL + _controllerName + '/countComment/' + id
     );
  }
}
