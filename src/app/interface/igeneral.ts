export class User{
  Name?:string;
  Email?:string
}
export class UserDetail {
  Id!: string;
  Name?: string
  Email?: string
  LastLogin?: string
}

export interface PersonalInfo {
  Id: string
  Name: string
  Email:string 
  SchoolName:string
  PhoneNumber:string
  Major:string
  Minor:string
}
export interface  MCTForm {
  UserId?:number,
  StudentProfileId?: number,
  CategoryId?: string,
  OtherCategory?:string,
  ActivityId?: string,
  OtherActivity?:string,
  PositionId?: string,
  OtherPosition?:string,
  InternalExternalEvent?: number,
  FinalSubmit?: number,
  OrganizationName?: string,
  CountryId?: number,
  StateId?: number,
  CityId?: number,
  WebAdress?: string,
  Image?: string,
  SupervisorName?: string
  SupervisorDepartment?: string
  SupervisorEmail?:string
  InternalFormImage?:string
}
export interface SuccessResponse {
  ResponseCode:string;
  ResponseText:string;
}

export interface activity {
  id: number,
  studentProfileId: number,
  categoryId: number,
  categoryName: string,
  otherCategory: string,
  activityId: number,
  activityName: string,
  otherActivity: string,
  positionId: number,
  positionName: string,
  otherPosition: string
  FromMonth:string,
  FromYearId:string,
  ToMonth:string,
  ToYearId:number
  internalExternal: number

  internal: Internal[],
  external: External[]
}
export interface Internal {
  id:number ,
  mctFormId:number ,
  supervisorName: string,
  supervisorDepartment: string,
  supervisorEmail: string,
  image: string
}
export interface External {
  id: number,
  mctFormId: number,
  organizationName: string,
  webAddress: string,
  image: string,
  countryId: number,
  countryName: string,
  stateId: string,
  stateName: string,
  cityId: number,
  cityName: string
}