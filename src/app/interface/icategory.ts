export interface ICategory {
  Id: number
  CategoryName: string
  Description: string
  ActivityLabelName: string
  SupervisingDepartment: string
  Type: string
  IsActive: number
}
