import { Component, Input, OnInit } from '@angular/core';
import { FormGroup, FormGroupDirective } from '@angular/forms';
import { GeneralService } from '../services/general.service';

@Component({
  selector: 'app-external',
  templateUrl: './external.component.html',
  styleUrls: ['./external.component.scss']
})
export class ExternalComponent implements OnInit {

  @Input() formGroupName!: string;
  form!:FormGroup;
  public countries: any;
  public states: any;
  public cities: any;
  constructor(private generalService:GeneralService,private rootFormGroup:FormGroupDirective) { }
 
 
  ngOnInit(): void {
    this.form = this.rootFormGroup.control.get(this.formGroupName) as FormGroup
    this.getAllCountries();
  }
  
  countryOnChangeEvent($event:any){
    this.getAllStatesByCountryId($event.value); 
  }
  stateOnChangeEvent($event:any) {
   this.getAllCitiesByStateId($event.value);
  }
  getAllCountries(){
    this.generalService.getAllCountries().subscribe(data => {
       this.countries = data;
    });
  }
  getAllStatesByCountryId(Id:any) {
    this.generalService.getAllStatesByCountryId(Id).subscribe(data=> {
      this.states = data;
    });
  }
  getAllCitiesByStateId(Id:any){
    this.generalService.getAllCitiesByStateId(Id).subscribe(data => {
     this.cities = data;
    });
  }
}
