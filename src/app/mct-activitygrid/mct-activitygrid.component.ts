import { animate, state, style, transition, trigger } from '@angular/animations';
import { Component, OnInit, ViewEncapsulation } from '@angular/core';
import { MatTableDataSource } from '@angular/material/table';
import { Router } from '@angular/router';
import { GeneralService } from '../services/general.service';
import Swal from 'sweetalert2'
import { ActivityService } from '../services/activity.service';
import { ActivatedRoute } from "@angular/router";
import { LocalStorageService } from "src/app/services/local-storage.service";

@Component({
  selector: "app-mct-activitygrid",
  templateUrl: "./mct-activitygrid.component.html",
  styleUrls: ["./mct-activitygrid.component.scss"],
  animations: [
    trigger("detailExpand", [
      state("collapsed", style({ height: "0px", minHeight: "0" })),
      state("expanded", style({ height: "*" })),
      transition(
        "expanded <=> collapsed",
        animate("225ms cubic-bezier(0.4, 0.0, 0.2, 1)")
      ),
    ]),
  ],
})
export class MctActivitygridComponent implements OnInit {
  public activity: any;
  public CountComment: any = [];
  dataStudentsList = new MatTableDataSource();
  user: any;
  Id:any;
  displayedStudentsColumnsList: string[] = [
    "Category",
    "Activity",
    "Position",
    "StudentLIFE",
    "View",
  ];
  isTableExpanded = false;
  constructor(
    private router: Router,
    private _generalService: GeneralService,
    private _activiyService: ActivityService,
    private _Activatedroute: ActivatedRoute,
    private localStorageService: LocalStorageService
  ) {}

  ngOnInit(): void {
    this.Id = this._Activatedroute.snapshot.paramMap.get("id");
    this.user = JSON.parse(
      this.localStorageService.get(this.Id)
    );
    this.getMctFormByStudentId();
  }

  gotoMCTForm() {
    this.router.navigateByUrl("/mctform/"+this.Id);
  }

  getMctFormByStudentId() {
    this._generalService.getMctFormByUserId(this.user.id).subscribe((data) => {
      this.dataStudentsList.data = data;
    });
  }

  toggleTableRows() {
    this.isTableExpanded = !this.isTableExpanded;

    this.dataStudentsList.data.forEach((row: any) => {
      row.isExpanded = this.isTableExpanded;
    });
  }
  applyFilter(event: Event) {
    const filterValue = (event.target as HTMLInputElement).value;
    this.dataStudentsList.filter = filterValue.trim().toLowerCase();
  }
  delete(id: number) {
    Swal.fire({
      title: "Are you sure?",
      text: "You won't be able to remove this!",
      icon: "warning",
      showCancelButton: true,
      confirmButtonColor: "#3085d6",
      cancelButtonColor: "#d33",
      confirmButtonText: "Yes, delete it!",
    }).then((result) => {
      if (result.isConfirmed) {
        this._activiyService.DisableMctForm(id).subscribe((data) => {
          if (data["responseCode"] != "01") {
            Swal.fire("Deleted!", data["responseText"], "success");
            this.getMctFormByStudentId();
          } else {
            Swal.fire("Record Not Found!", data["responseText"], "error");
          }
        });
      }
    });
  }
}
