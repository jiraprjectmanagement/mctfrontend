import { ComponentFixture, TestBed } from '@angular/core/testing';

import { MctActivitygridComponent } from './mct-activitygrid.component';

describe('MctActivitygridComponent', () => {
  let component: MctActivitygridComponent;
  let fixture: ComponentFixture<MctActivitygridComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ MctActivitygridComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(MctActivitygridComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
