import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { MsalGuard } from '@azure/msal-angular';
import { LoginComponent } from './login/login.component';
import { MaslGuard } from './masl.guard';
import { MctActivitygridComponent } from './mct-activitygrid/mct-activitygrid.component';
import { MctFormComponent } from './mct-form/mct-form.component';
import { TermsConditionsComponent } from './terms-conditions/terms-conditions.component';
import { UpdateMctFormComponent } from './update-mct-form/update-mct-form.component';

const routes: Routes = [
  { path: "login", component: LoginComponent },
  {
    path: "termcondition/:id",
    component: TermsConditionsComponent,
    //canActivate: [MaslGuard],
  },
  { path: "mctform/:id", component: MctFormComponent, canActivate: [MaslGuard] },
  {
    path: "mctactivitygrid/:id",
    component: MctActivitygridComponent,
    canActivate: [MaslGuard],
  },
  
  {
    path: "update-mctform/:id",
    component: UpdateMctFormComponent,
    canActivate: [MaslGuard],
  },
  { path: "**", redirectTo: "login", pathMatch: "full",canDeactivate:[MsalGuard] },
];
const isIframe = window !== window.parent && !window.opener;
@NgModule({
  imports: [
    RouterModule.forRoot(routes, {
      initialNavigation: !isIframe ? "enabled" : "disabled", // Don't perform initial navigation in iframes
    }),
  ],
  exports: [RouterModule],
})
export class AppRoutingModule {}
