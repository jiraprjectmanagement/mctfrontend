import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { MsalService } from '@azure/msal-angular';
import { AuthenticationResult } from '@azure/msal-browser';
import { User, UserDetail } from '../interface/igeneral';
import { AuthService } from '../services/auth.service';
import { LocalStorageService } from '../services/local-storage.service';

@Component({
  selector: "app-login",
  templateUrl: "./login.component.html",
  styleUrls: ["./login.component.scss"],
})
export class LoginComponent implements OnInit {
  isIframe = false;
  loginDisplay = false;

  constructor(
    private router: Router,
    private msalService: MsalService,
    private authService: AuthService,
    private localStorageService: LocalStorageService
  ) {}

  ngOnInit(): void {
    this.msalService.instance.handleRedirectPromise().then((res) => {
      if (res != null && res.account != null) {
        this.msalService.instance.setActiveAccount(res.account);
      }
    });
  }

  isLoggedIn(): boolean {
    return this.msalService.instance.getActiveAccount() != null;
  }

  login() {
    this.msalService
      .loginPopup()
      .subscribe((response: AuthenticationResult) => {
        this.msalService.instance.setActiveAccount(response.account);
        console.log(response.account);
        if (response.account?.idTokenClaims) {
          let User = {
            Name: response.account?.name,
            Email: response.account?.username,
          };
          this.authService.CreateUserRecord(User).subscribe((data: any)=> {
             console.warn(data);
              this.authService
                .getUserDetailsByEmail(response.account?.username)
                .subscribe((data:any) => {
                  data.forEach((element:any) => {
                    this.localStorageService.set(element.id,element);
                       if (this.localStorageService.isLocalStorageSupported) {
                           this.gotoMctTermCondition(element.id);
                       } else {
                         this.logout();
                       }
                    });
                });
          });
        } 
        else {
          this.router.navigateByUrl("/login");
        }
      });
  }

  logout() {
    this.localStorageService.remove('1');
    this.msalService.logout();
  }

  gotoMctTermCondition(id:number) {
    this.router.navigateByUrl("/termcondition/"+id);
  }
}
