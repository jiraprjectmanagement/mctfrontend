import { Component, Input, OnInit } from '@angular/core';
import { FormGroup, FormGroupDirective } from '@angular/forms';
import { ActivityService } from 'src/app/services/activity.service';
import { CategoryService } from 'src/app/services/category.service';
import { GeneralService } from 'src/app/services/general.service';


@Component({
  selector: 'app-form',
  templateUrl: './form.component.html',
  styleUrls: ['./form.component.scss']
})
export class FormComponent implements OnInit {

  @Input() formGroupName!: string;
  form!:FormGroup;
  Id?:any;
  description?: string = ''; 
  Isdisplay? : string = '';
  IsdisplyPosition: string  = '';
  IsdisplyActivity: string = '';
  fileds?: string = '';
  ActivitylabelName?: string;
  allCategory: any = [];
  allActivity: any = [];
  allPositions: any;

  allYears: any;
  value :any;
  constructor(private _rootFormGroup:FormGroupDirective,
              private _categoryService:CategoryService,
              private _activityService:ActivityService,
              private _generalService: GeneralService) {  
             
              }

  ngOnInit(): void {
    this.form = this._rootFormGroup.control.get(this.formGroupName) as FormGroup;
    this.conditionChecked();
  }

  ngAfterViewInit() {
    this.getAllCategory(); 
  }

  conditionChecked() {
    if(this.form.controls.CategoryName.value == "" || this.form.controls.CategoryName.value == null)
    {
     this.Isdisplay = 'otherCategory';
     this.form.controls['CategoryName'].patchValue('0');
    }
    if(this.form.controls.ActivityName.value == "" || this.form.controls.ActivityName.value == null)
    {
      this.IsdisplyActivity =  'OtherActivity';
      this.form.controls['ActivityName'].patchValue('0');
    }
    if(this.form.controls.OtherPosition.value == "" || this.form.controls.OtherPosition.value == null)
    {
      this.IsdisplyPosition = 'OtherPosition';
      this.form.controls['Position'].patchValue('0');
    }
  }
  
  CategoryOnChange(event: any){
    if(event.value != 'otherCategory') {
      this.Id = event.value;
       this.getAllActivityByCategoryId(event.value);
       this.getCategoryById(event.value);
      this.Isdisplay = '';
    } else {
        this.Isdisplay = 'otherCategory';
        this.form.controls['CategoryName'].patchValue('0');

    }
  }
  
  PositionOnChange(event:any) {
    if(event.value != 'OtherPosition'){
      this.IsdisplyPosition = '';
    }
    else{
       this.form.controls['Position'].patchValue('0');
       this.IsdisplyPosition = 'OtherPosition';
    }
  }
  
  ActivityOnChange(event:any) {
    if(event.value != 'OtherActivity'){
      //this.getAllPositionByCategoryId(event.value);
      this.IsdisplyActivity = '';
    }else{
      this.form.controls['ActivityName'].patchValue('0');
      this.IsdisplyActivity =  'OtherActivity';
    }
    this.getAllPosition();
  }
  
  getAllCategory() {
    this._categoryService.getAllCategory().subscribe(data => {
      if (Array.isArray(data) && data.length) {
         this.allCategory = data;
         this.getAllActivity();
      }
    });
  }
  
  getCategoryById(Id:any) {
    this._categoryService.getCategoryById(Id).subscribe(data => {
      this.description = data[0].description;
      this.fileds = data[0].fields;
      this.ActivitylabelName = data[0].activitylabelName;
    });
  }
  
  getAllActivityByCategoryId(Id:any){
    this._activityService.getAllActivityByCategoryId(Id).subscribe(data => {
       this.allActivity = data;
    });
  }

  getAllActivity(){
    this._activityService.getAllActivity().subscribe((data:any) => {
      if (Array.isArray(data) && data.length) {
         this.allActivity = data;
         this.getAllPosition();
      }
    });
  }

  getAllPosition() {
    this._activityService.getAllPosition().subscribe(data => {
      if (Array.isArray(data) && data.length) {
          this.allPositions = data;
          this.getAllYears();
     }
    });
  }  

  getAllPositionByCategoryId(Id:number) {
    this._activityService.getAllPositionByCategoryId(Number(this.Id)).subscribe(data => {
      this.allPositions = data;
    });
  }  
     
  getAllYears() {
    this._generalService.getAllYears().subscribe((data:any) => {
         this.allYears = data;
    });
  }
}

