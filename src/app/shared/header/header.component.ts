import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { MsalService } from '@azure/msal-angular';
import { MenuItem } from 'src/app/interface/menu-item';
import { LocalStorageService } from 'src/app/services/local-storage.service';
import { Router } from '@angular/router';
@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss'],
})
export class HeaderComponent implements OnInit {
  user: any;
  Id: any;
  menuItems: MenuItem[] = [
    {
      label: 'Activites',
      icon: 'menu',
      showOnMobile: false,
      showOnTablet: false,
      showOnDesktop: true,
    },
    {
      label: 'About',
      icon: 'help',
      showOnMobile: false,
      showOnTablet: true,
      showOnDesktop: true,
    },
    {
      label: 'Logout',
      icon: 'logout',
      showOnMobile: true,
      showOnTablet: true,
      showOnDesktop: true,
    },
    {
      label: 'Profile',
      icon: 'account_circle',
      showOnMobile: false,
      showOnTablet: false,
      showOnDesktop: false,
    },
  ];
  constructor(
    private msalService: MsalService,
    private localStorageService: LocalStorageService,
    private _Activatedroute: ActivatedRoute,
    private router: Router
  ) {}

  ngOnInit(): void {
    if (this.localStorageService.isLocalStorageSupported) {
      this.Id = this._Activatedroute.snapshot.paramMap.get('id');
      this.user = JSON.parse(this.localStorageService.get(this.Id));
    }
  }

  logout(menuName: string) {
    if (menuName == 'Logout') {
      this.msalService.logout();
      this.localStorageService.remove(this.Id);
    }
    if (menuName == 'Activites') {
      this.router.navigateByUrl('/mctactivitygrid/' + this.user.id);
    }
  }
}
