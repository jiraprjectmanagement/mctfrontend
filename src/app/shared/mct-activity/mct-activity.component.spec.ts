import { ComponentFixture, TestBed } from '@angular/core/testing';

import { MctActivityComponent } from './mct-activity.component';

describe('MctActivityComponent', () => {
  let component: MctActivityComponent;
  let fixture: ComponentFixture<MctActivityComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ MctActivityComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(MctActivityComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
