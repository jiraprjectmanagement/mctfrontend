import { Component, Input, OnInit } from '@angular/core';
import { CommentService } from 'src/app/services/comment.service';
import Swal from 'sweetalert2';
@Component({
  selector: 'app-comment',
  templateUrl: './comment.component.html',
  styleUrls: ['./comment.component.scss'],
})
export class CommentComponent implements OnInit {
  @Input () id: any;
  public allComments!: Array<any>;
  constructor(private _commentService: CommentService) {}

  ngOnInit(): void {
    this.getAllCommentByMctFormId(this.id);
  }

  resolveComment() {
    let IComment = {
      UserId:this.id
    }
  const swalWithBootstrapButtons = Swal.mixin({
    customClass: {
      confirmButton: 'btn btn-success',
      cancelButton: 'btn btn-danger',
    },
    buttonsStyling: false,
  });

  swalWithBootstrapButtons
    .fire({
      title: 'Are you sure?',
      text: "You won't be able to resolve this comment!",
      icon: 'warning',
      showCancelButton: true,
      confirmButtonText: 'Yes, delete it!',
      cancelButtonText: 'No, cancel!',
      reverseButtons: true,
    })
    .then((result) => {
      if (result.isConfirmed) {
        this._commentService.ResolvedCommentById(this.id, IComment).subscribe((data:any)=> {
            if(data ==  1) {
              swalWithBootstrapButtons.fire(
                'Resloved!',
                'Your comment has been resolved.',
                'success'
              );
                 this.getAllCommentByMctFormId(this.id);
            }
        });
      } else if (
        /* Read more about handling dismissals below */
        result.dismiss === Swal.DismissReason.cancel
      ) {
        swalWithBootstrapButtons.fire(
          'Cancelled',
          'Your comment not resolved :)',
          'error'
        );
      }
    });
  }

  getAllCommentByMctFormId(id: number) {
    this._commentService.getAllCommentByMctFormId(id).subscribe((data: any) => {
      this.allComments = data;
    })
  }
}
