import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { ActivatedRoute } from "@angular/router";
import Swal from "sweetalert2";
@Component({
  selector: "app-terms-conditions",
  templateUrl: "./terms-conditions.component.html",
  styleUrls: ["./terms-conditions.component.scss"],
})
export class TermsConditionsComponent implements OnInit {
  constructor(
    private router: Router,
    private _Activatedroute: ActivatedRoute
  ) {}

  ngOnInit(): void {}
  acceptTermsAndConditions() {
    Swal.fire({
      title: "Are you sure?",
      text: "You won't to accept term and condition!",
      icon: "warning",
      showCancelButton: true,
      confirmButtonColor: "#3085d6",
      cancelButtonColor: "#d33",
      confirmButtonText: "Yes, accept it!",
    }).then((result) => {
      if (result.isConfirmed) {
        Swal.fire("Accepted!", "Accepted term and conditions", "success");
        this.gotoActivitygrid();
      }
    });
  }
  gotoActivitygrid() {
    this.router.navigateByUrl(
      "/mctactivitygrid/" + this._Activatedroute.snapshot.paramMap.get("id")
    );
  }
}
