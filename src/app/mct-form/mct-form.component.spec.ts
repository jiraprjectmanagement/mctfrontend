import { ComponentFixture, TestBed } from '@angular/core/testing';

import { MctFormComponent } from './mct-form.component';

describe('MctFormComponent', () => {
  let component: MctFormComponent;
  let fixture: ComponentFixture<MctFormComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ MctFormComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(MctFormComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
