import {Component, OnInit,Output,EventEmitter} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {BreakpointObserver} from '@angular/cdk/layout';
import {StepperOrientation} from '@angular/material/stepper';
import {Observable} from 'rxjs';
import {map} from 'rxjs/operators';
import { CategoryService } from '../services/category.service';
import { ActivityService } from '../services/activity.service';
import { IActivity } from '../interface/iactivity';
import { GeneralService } from '../services/general.service';
import { ActivatedRoute } from '@angular/router';
import { LocalStorageService } from "src/app/services/local-storage.service";
import Swal from 'sweetalert2'


@Component({
  selector: 'app-mct-form',
  templateUrl: './mct-form.component.html',
  styleUrls: ['./mct-form.component.scss'],
})
export class MctFormComponent implements OnInit {
  MCTActivitiesFormGroup!: FormGroup;
  PersonalInformationFormGroup!: FormGroup;
  UnderTakingFormGroup!: FormGroup;
  isShow: number = 0;
  private userId: any;
  private user: any;
  protected Id?: number;
  public disableInputField: boolean = false;
  public allCategory: any = [];
  public allActivity: any = [];
  public description?: string;
  public fileds?: string;
  public ActivitylabelName?: string;
  public ActivityModel!: IActivity;
  public allPositions?: any = [];
  public allYears?: any = [];
  public UserInfo: any = [];
  public UserInfoIntenal: any = [];
  public Isdisplay?: string;
  public IsdisplyActivity?: string;
  public IsdisplyPosition?: string;

  stepperOrientation: Observable<StepperOrientation>;

  constructor(
    private _formBuilder: FormBuilder,
    breakpointObserver: BreakpointObserver,
    private localStorageService: LocalStorageService,
    private _Activatedroute: ActivatedRoute,
    private categoryService: CategoryService,
    private activityService: ActivityService,
    private generalService: GeneralService
  ) {
    this.stepperOrientation = breakpointObserver
      .observe('(min-width: 800px)')
      .pipe(map(({ matches }) => (matches ? 'horizontal' : 'vertical')));
  }

  ngOnInit(): void {
    this.buildForm();
    this.getAllCategory();
    this.getAllYears();
    if (this.localStorageService.isLocalStorageSupported) {
      this.userId = this._Activatedroute.snapshot.paramMap.get('id');
      this.user = JSON.parse(this.localStorageService.get(this.userId));
      this.getGetPersonDataFromAd(this.user.email);
    }
  }
  buildForm(): void {
    this.PersonalInformationFormGroup = this._formBuilder.group({
      UserId: [this._Activatedroute.snapshot.paramMap.get('id')],
      Id: ['', Validators.required],
      Name: ['', Validators.required],
      Email: ['', Validators.required],
      PhoneNumber: ['', Validators.required],
      SchoolName: ['', Validators.required],
      Major: ['', Validators.required],
      Minor: ['', Validators.required],
    });

    this.MCTActivitiesFormGroup = this._formBuilder.group({
      MctBasicForm: this._formBuilder.group({
        CategoryName: [''],
        OtherCategory: [''],
        ActivityName: [''],
        OtherActivity: [''],
        Position: [''],
        OtherPosition: [''],
        FromMonth: ['', Validators.required],
        FromYear: ['', Validators.required],
        ToMonth: ['', Validators.required],
        ToYear: ['', Validators.required],
        EventLocation: ['', Validators.required],
      }),
      ExternalBasicFrom: this._formBuilder.group({
        OrganizationName: [''],
        Countrydropdown: [''],
        Statedropdown: [''],
        Citydropdown: [''],
        WebAddress: [''],
        Image: [''],
      }),
      InternalBasicFrom: this._formBuilder.group({
        SupervisorName: [''],
        SupervisorDepartment: [''],
        SupervisorEmail: [''],
        InternalFormImage: [''],
      }),
    });

    this.UnderTakingFormGroup = this._formBuilder.group({
      UnderTakingSubmit: [''],
    });
  }

  activityLocationInternal(): void {
    this.isShow = 1;
    this.MCTActivitiesFormGroup.controls.ExternalBasicFrom.reset();
  }

  activityLocationExternal(): void {
    this.isShow = 2;
    this.MCTActivitiesFormGroup.controls.InternalBasicFrom.reset();
  }

  CategoryOnChange(event: any) {
    if (event.value != 'otherCategory') {
      this.Id = event.value;
      this.getCategoryById(event.value);
      this.getAllActivityByCategoryId(event.value);
      this.Isdisplay = '';
    } else {
      this.Isdisplay = 'otherCategory';
    }
  }

  ActivityOnChange(event: any) {
    if (event.value != 'OtherActivity') {
      this.getAllPositionByCategoryId();
      this.getAllYears();
      this.IsdisplyActivity = '';
    } else {
      this.IsdisplyActivity = 'OtherActivity';
    }
  }

  PositionOnChange(event: any) {
    if (event.value == 'OtherPosition') {
      this.IsdisplyPosition = 'OtherPosition';
    } else {
      this.IsdisplyPosition = '';
    }
  }

  getGetPersonDataFromAd(email: string) {
    this.generalService.getPersonDataFromAD(email).subscribe((data) => {
      this.UserInfo = JSON.parse(data);

      this.generalService
        .getProfileIfExists(this.UserInfo.EmplId)
        .subscribe((data) => {
          if (data[0] != null) {
            this.UserInfoIntenal = data[0];
            this.PersonalInformationFormGroup.patchValue({
              Id: this.UserInfoIntenal.id,
              Name: this.UserInfoIntenal.name,
              Email: this.UserInfoIntenal.email,
              SchoolName: this.UserInfoIntenal.schoolName,
              PhoneNumber: this.UserInfoIntenal.phoneNumber,
              Major: this.UserInfoIntenal.major,
              Minor: this.UserInfoIntenal.minor,
            });
            this.disableInputField = true;
          } else {
            this.PersonalInformationFormGroup.patchValue({
              Id: this.UserInfo.EmplId,
              Name: this.UserInfo.DisplayName,
              Email: this.UserInfo.Email,
              SchoolName: this.UserInfo.Department,
            });
          }
        });
    });
  }

  getAllCategory() {
    this.categoryService.getAllCategory().subscribe((data) => {
      this.allCategory = data;
    });
  }

  getCategoryById(Id: any) {
    this.categoryService.getCategoryById(Id).subscribe((data) => {
      this.description = data[0].description;
      this.fileds = data[0].fields;
      this.ActivitylabelName = data[0].activitylabelName;
    });
  }

  getAllActivityByCategoryId(Id: any) {
    this.activityService.getAllActivityByCategoryId(Id).subscribe((data) => {
      this.allActivity = data;
    });
  }

  getAllPositionByCategoryId() {
    this.activityService
      .getAllPositionByCategoryId(Number(this.Id))
      .subscribe((data) => {
        this.allPositions = data;
      });
  }

  getAllYears() {
    this.generalService.getAllYears().subscribe((data) => {
      this.allYears = data;
    });
  }

  SubmitPersonlInfo() {
    if (!this.PersonalInformationFormGroup.invalid) {
      this.generalService
        .CreatePersonalInfo(this.PersonalInformationFormGroup.value)
        .subscribe((data) => {
        });
    } else {
      Swal.fire({
        icon: 'error',
        title: 'Oops...',
        text: 'some data are missing! in your profile',
        footer: '<a href="">Why do I have this issue?</a>',
      });
      this.getGetPersonDataFromAd(this.user.email);
    }
  }

  onSave(submit: number) {
    if (this.MCTActivitiesFormGroup.controls.MctBasicForm.valid)
    {
    let countryId: number = 0;
    let stateId: number = 0;
    let cityId: number = 0;
    let categoryName =
      this.MCTActivitiesFormGroup.controls.MctBasicForm.value.CategoryName;
    let activityName =
      this.MCTActivitiesFormGroup.controls.MctBasicForm.value.ActivityName;
    let positionName =
      this.MCTActivitiesFormGroup.controls.MctBasicForm.value.Position;
    if (categoryName === 'otherCategory') {
      categoryName = '0';
    }
    if (activityName === 'OtherActivity') {
      activityName = '0';
    }
    if (positionName === 'OtherPosition') {
      positionName = '0';
    }
    if (
      this.MCTActivitiesFormGroup.controls.MctBasicForm.value.EventLocation !=
      '1'
    ) {
      countryId =
        this.MCTActivitiesFormGroup.controls.ExternalBasicFrom.value
          .Countrydropdown;
      stateId =
        this.MCTActivitiesFormGroup.controls.ExternalBasicFrom.value
          .Statedropdown;
      cityId =
        this.MCTActivitiesFormGroup.controls.ExternalBasicFrom.value
          .Citydropdown;
    }

    let MCTForm = {
      UserId: this.user.id,
      StudentProfileId: this.PersonalInformationFormGroup.value.Id,
      CategoryId: categoryName,
      OtherCategory:
        this.MCTActivitiesFormGroup.controls.MctBasicForm.value.OtherCategory,
      ActivityId: activityName,
      OtherActivity:
        this.MCTActivitiesFormGroup.controls.MctBasicForm.value.OtherActivity,
      PositionId: positionName,
      OtherPosition:
        this.MCTActivitiesFormGroup.controls.MctBasicForm.value.OtherPosition,
      FromMonth:
        this.MCTActivitiesFormGroup.controls.MctBasicForm.value.FromMonth,
      FromYearId:
        this.MCTActivitiesFormGroup.controls.MctBasicForm.value.FromYear,
      ToMonth: this.MCTActivitiesFormGroup.controls.MctBasicForm.value.ToMonth,
      ToYearId: this.MCTActivitiesFormGroup.controls.MctBasicForm.value.ToYear,
      InternalExternalEvent:
        this.MCTActivitiesFormGroup.controls.MctBasicForm.value.EventLocation,
      FinalSubmit: submit,
      OrganizationName:
        this.MCTActivitiesFormGroup.controls.ExternalBasicFrom.value
          .OrganizationName,
      CountryId: countryId,
      StateId: stateId,
      CityId: cityId,
      WebAdress:
        this.MCTActivitiesFormGroup.controls.ExternalBasicFrom.value.WebAddress,
      Image: this.MCTActivitiesFormGroup.controls.ExternalBasicFrom.value.Image,
      SupervisorName:
        this.MCTActivitiesFormGroup.controls.InternalBasicFrom.value
          .SupervisorName,
      SupervisorDepartment:
        this.MCTActivitiesFormGroup.controls.InternalBasicFrom.value
          .SupervisorDepartment,
      SupervisorEmail:
        this.MCTActivitiesFormGroup.controls.InternalBasicFrom.value
          .SupervisorEmail,
      InternalFormImage:
        this.MCTActivitiesFormGroup.controls.InternalBasicFrom.value
          .InternalFormImage,
    };
     this.generalService.createMCTForm(MCTForm).subscribe((data:any)=> {
      if(data["responseCode"]  == "00") {
        this.MCTActivitiesFormGroup.reset();
        Swal.fire('Activity', data["responseText"], 'success')
      }else {
        this.MCTActivitiesFormGroup.reset();
        Swal.fire('Oops...', data["responseText"], 'error')
      }
    });

  }//1stIf END
  else {
  Swal.fire({
    icon: 'error',
    title: 'Oops...',
    text: 'some data are missing in activity section!',
    footer: '<a href="">Why do I have this issue?</a>',
   });
   }//1st if else close
  } //onSave END
}
